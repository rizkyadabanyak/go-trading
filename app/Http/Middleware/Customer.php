<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->token == null){
            return response()->json('error');
        }
        $data = \App\Models\Customer::where('token',$request->token)->count();

        if ($data == 0){
            return response()->json('error');
        }
        return $next($request);    }
}
