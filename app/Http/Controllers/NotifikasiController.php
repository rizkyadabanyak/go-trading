<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\CustomerNotice;
use App\Models\Materi;
use App\Models\Notice;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class NotifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Notice::all();
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="notifikasi/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.notifikasiDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.notifikasi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.notifikasi.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Notice();
        $data->value = $request->value;

        $data->save();

        $customers = Customer::all();
//        dd($customers);
        foreach ($customers as $customer){
            $noticeCustomer = new CustomerNotice();
            $noticeCustomer->customer_id = $customer->id;
            $noticeCustomer->notice_id = $data->id;

            $noticeCustomer->save();
        }
        return redirect()->route('admin.notifikasi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Notice::find($id);
        view()->share([
            'data' => $data,
        ]);
        return view('admin.content.notifikasi.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Notice::find($id);
        $data->value = $request->value;
        $data->save();

        return redirect()->route('admin.notifikasi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataDelete = CustomerNotice::where([
            'notice_id' => $id,
        ])->delete();


        $data = Notice::find($id);

        $data->delete();
        return redirect()->route('admin.notifikasi.index')->withSucsess('berhasil di delete');
    }
}
