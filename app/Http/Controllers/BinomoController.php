<?php

namespace App\Http\Controllers;

use App\Models\Binomo;
use App\Models\Link;
use Illuminate\Http\Request;

class BinomoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Binomo::first();
        view()->share([
            'data' => $data
        ]);

        return view('admin.content.binomo.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.binomo.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Binomo();
        $data->link = $request->link;

        $data->save();

        return redirect()->route('admin.binomo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Binomo::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.binomo.edit');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Binomo::find($id);
        $data->link = $request->link;

        $data->save();

        return redirect()->route('admin.binomo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Binomo::find($id);
        $data->delete();

        return redirect()->back();
    }
}
