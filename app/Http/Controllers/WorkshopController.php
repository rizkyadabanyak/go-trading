<?php

namespace App\Http\Controllers;

use App\Models\Materi;
use App\Models\Workshop;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Workshop::all();
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="workshop/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.workshopDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.workshop.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.workshop.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Workshop();
        $data->link = $request->link;
        $data->value = $request->value;

        $data->save();

        return redirect()->route('admin.workshop.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Workshop::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.workshop.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Workshop::find($id);
        $data->link = $request->link;
        $data->value = $request->value;
        $data->save();

        return redirect()->route('admin.workshop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Workshop::find($id);

        $data->delete();
        return redirect()->route('admin.workshop.index')->withSucsess('berhasil di delete');
    }
}
