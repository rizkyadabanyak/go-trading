<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Customer;
use App\Models\CustomerNotice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\Concerns\Has;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Customer::all();
            return DataTables::of($data)->addColumn('img', function($data){
//                $img = '<img src="'.asset($data->img).'">';
                $img = '<img src="'.asset($data->img).'" class="img-fluid img-thumbnail">';
                return $img;
            })
                ->addColumn('action', function($data){
                    $button = '<a href="customers/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.customersDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    $button .= '<a href="'.route('admin.changePassword',$data->id).'" class="btn btn-warning"><i class="fa fa-pencil"></i> Password</a>';
                    return $button;
                })
                ->rawColumns(['img','action'])
                ->make(true);
        }
        return view('admin.content.customers.customer');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.customers.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Customer();

        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();

        return redirect()->route('admin.customers.index')->withSucsess('berhasil di tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Customer::find($id);
        view()->share([
           'data' => $data
        ]);
        return view('admin.content.customers.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Customer::find($id);

        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;

            $imgDB = 'uploads/'.$newName;
            $request->image->move(public_path('uploads'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $request->newImage;
        }

        $data->name = $request->name;
        $data->email = $request->email;
        $data->city = $request->city;
        $data->deposit = $request->deposit;
        $data->penarikan = $request->penarikan;
        $data->whatsapp = $request->whatsapp;
        $data->save();

        return redirect()->route('admin.customers.index')->withSucsess('berhasil di save');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataDelete = CustomerNotice::where([
            'customer_id' => $id,
        ])->delete();

        $data = Customer::find($id);

        $data->delete();
        return redirect()->route('admin.customers.index')->withSucsess('berhasil di delete');
    }
    public function change($id){
        $data = Customer::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.customers.change');
    }

    public function changePassword(Request $request,$id){



        $data = Customer::find($id);
        $data->password = Hash::make($request->password);
        $data->save();

        return redirect()->route('admin.customers.index')->withSucsess('berhasil di update');
    }
}
