<?php

namespace App\Http\Controllers\Apiv1;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfilController extends Controller
{
    public function getProfile($id){
        $data = Customer::find($id);
        $date = $data->created_at->format('M Y');

        return response()->json([
            'data' =>$data,
            'date' => $date
        ]);
    }


    public function profile(Request $request,$id){
        $data = Customer::find($id);

        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads'), $newName);
            $data->img = $imgDB;
        }

        $data->name = $request->name;
        $data->city = $request->city;
        $data->whatsapp = $request->whatsapp;
        $data->status = '1';

        $data->save();

        return response()->json($data);
    }
    public function getImg($id){
        $data = Customer::find($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.customers.img');
    }
    public function img(Request $request,$id){
        $data = Customer::find($id);

        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;

            $imgDB = 'uploads/'.$newName;
            $request->image->move(public_path('uploads'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $request->newImage;
        }

        $data->save();

        return redirect()->back()->withSuccess('berhasil di update');
    }

}
