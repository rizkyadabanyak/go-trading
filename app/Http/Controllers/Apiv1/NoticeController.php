<?php

namespace App\Http\Controllers\Apiv1;

use App\Http\Controllers\Controller;
use App\Http\Resources\NoticeResource;
use App\Models\CustomerNotice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function getNotifikasi ($id){
        $notifikasi = CustomerNotice::whereCustomerId($id)->get();

        $count = CustomerNotice::whereCustomerId($id)->whereStatus('0')->count();

        for ($i = 1;$i <= $count;$i++){
            $tmp = CustomerNotice::whereCustomerId($id)->whereStatus('0')->first();
            $tmp->status = '1';

            $tmp->save();
        }



        return NoticeResource::collection($notifikasi);

//        return response()->json($notifikasi->value);
    }
    public function getNotifikasiCount($id){
        $count = CustomerNotice::whereCustomerId($id)->whereStatus('0')->count();
        return response()->json($count);
    }
}
