<?php

namespace App\Http\Controllers\Apiv1;

use App\Http\Controllers\Controller;
use App\Models\Binomo;
use App\Models\Carousel;
use App\Models\CustomerNotice;
use App\Models\Link;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $carousels = Carousel::all();

        return response()->json($carousels);
    }
    public function binomo(){
        $link = Binomo::first();
        return response()->json($link);
    }
    public function yutub(){
        $yt = Link::first();

        return response()->json($yt);
    }
}
