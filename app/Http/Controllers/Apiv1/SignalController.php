<?php

namespace App\Http\Controllers\Apiv1;

use App\Http\Controllers\Controller;
use App\Models\Signal;
use Illuminate\Http\Request;

class SignalController extends Controller
{
    public function signal(){
        $data = Signal::first();

        return response()->json($data);
    }
}
