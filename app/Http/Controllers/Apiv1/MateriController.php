<?php

namespace App\Http\Controllers\Apiv1;

use App\Http\Controllers\Controller;
use App\Models\Materi;
use App\Models\Promosi;
use App\Models\Workshop;
use Illuminate\Http\Request;

class MateriController extends Controller
{
    public function materi(){
        $basics = Materi::whereCategoryId(1)->get();

        return response()->json($basics);
    }
    public function workshop(){
        $workshops = Workshop::all();
        return response()->json($workshops);
    }
    public function promosi(){
        $promosis = Promosi::all();
        return response()->json($promosis);
    }
    public function intermediate(){
        $intermediates = Materi::whereCategoryId(2)->get();
//        $materies = Materi::all();
        return response()->json($intermediates);
    }
    public function advance(){
        $advances = Materi::whereCategoryId(3)->get();

        return response()->json($advances);
    }
}
