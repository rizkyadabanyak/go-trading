<?php

namespace App\Http\Controllers\Apiv1;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $user = Customer::whereEmail($request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            $user->token = Hash::make($request->email);
            $user->save();

            return response()->json($user);
        }

        return response()->json([
            'message' => 'error'
        ], 401);
    }

    public function regis(Request $request)
    {
        $data = new Customer();

        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'email' => 'required',
            'nohp' => 'required',
            'password' => 'required',
        ]);

        $data->token = Hash::make($request->email);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->nohp = $request->nohp;
        $data->password = Hash::make($request->password);
        $data->city = $request->city;

        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $data = Customer::where('id', $id)->first();

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'nohp' => 'required',
            'instansi' => 'required'
        ]);

        $data->name = $request->name;
        $data->email = $request->email;
        $data->nohp = $request->nohp;
        $data->instansi = $request->instansi;

        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }

    public function updatePassword(Request $request, $id)
    {
        $data = Customer::where('id', $id)->first();

        $request->validate([
            'password' => 'required',

        ]);
        dd($data);
        $data->password = Hash::make($request->password);
        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }
    public function updateUpdateImg(Request $request, $id)
    {
        $data = Customer::where('id', $id)->first();

        $request->validate([
            'img' => 'required',

        ]);

        $data->img = 'image/'.$request->img;

        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }
    public function logout(Request $request){
        $data = Customer::whereToken($request->token)->first();
        $data->token = null;
        $data->save();

        return response()->json([
            'message' => 'berhasil logout'
        ], 200);
    }
}
