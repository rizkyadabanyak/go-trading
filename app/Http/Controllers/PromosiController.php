<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\Materi;
use App\Models\Promosi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class PromosiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Promosi::all();
            return DataTables::of($data)->addColumn('img', function($data){
//                $img = '<img src="'.asset($data->img).'">';
                $img = '<img src="'.asset($data->img).'" class="img-fluid img-thumbnail">';
                return $img;
            })
                ->addColumn('action', function($data){
                    $button = '<a href="promosi/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.promosiDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['img','action'])
                ->make(true);
        }

        return view('admin.content.promosi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.promosi.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Promosi();
        $file = $request->file('image');

        $name = rand(999999,1);
        $extension = $file->getClientOriginalExtension();
        $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
        $imgDB = 'uploads/'.$newName;
        $request->image->move(public_path('uploads'), $newName);
        $data->img = $imgDB;
        $data->value = $request->value;

        $data->save();

        return redirect()->route('admin.promosi.index')->withSucsess('berhasil di save');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Promosi::find($id);

        view()->share([
            'data' => $data
        ]);
        return view('admin.content.promosi.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Promosi::find($id);

        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;
            $request->image->move(public_path('uploads'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $request->newImage;
        }
        $data->value = $request->value;

        $data->save();

        return redirect()->route('admin.promosi.index')->withSucsess('berhasil di save');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Promosi::find($id);

        $data->delete();
        return redirect()->route('admin.promosi.index')->withSucsess('berhasil di delete');
    }
}
