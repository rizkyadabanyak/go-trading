<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\Category;
use App\Models\Link;
use App\Models\Materi;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Materi::all();
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="materi/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.materiDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.materi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Category::all();
        view()->share([
           'categories' => $data
        ]);
        return view('admin.content.materi.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Materi();
        $data->link = $request->link;
        $data->value = $request->value;
        $data->category_id = $request->category_id;

        $data->save();


        return redirect()->route('admin.materi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Materi::find($id);
        $categories = Category::all();
        view()->share([
            'data' => $data,
            'categories' => $categories

        ]);
        return view('admin.content.materi.edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Materi::find($id);
        $data->link = $request->link;
        $data->value = $request->value;
        $data->category_id = $request->category_id;

        $data->save();

        return redirect()->route('admin.materi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Materi::find($id);

        $data->delete();
        return redirect()->route('admin.materi.index')->withSucsess('berhasil di delete');
    }
}
