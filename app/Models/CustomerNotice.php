<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerNotice extends Model
{
    use HasFactory;
//    protected $with = ['getMessages'];

    public function getMessages(){
        return $this->belongsTo(Notice::class,'notice_id');
    }
}
