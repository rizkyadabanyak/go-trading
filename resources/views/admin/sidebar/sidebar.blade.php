<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="">Trading</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">trd</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li><a class="nav-link" href="{{route('admin.customers.index')}}"><i class="fas fa-users"></i> <span>Customer</span></a></li>
            <li><a class="nav-link" href="{{route('admin.link.index')}}"><i class="fas fa-columns"></i> <span>Link</span></a></li>
            <li><a class="nav-link" href="{{route('admin.notifikasi.index')}}"><i class="fas fa-columns"></i> <span>Notifikasi</span></a></li>
            <li><a class="nav-link" href="{{route('admin.binomo.index')}}"><i class="fas fa-columns"></i> <span>Binomo</span></a></li>
            <li><a class="nav-link" href="{{route('admin.signal.index')}}"><i class="fas fa-columns"></i> <span>Signal</span></a></li>

            <li class="nav-item dropdown ">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Tambah Data</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.carousels.index')}}">Carousel</a></li>
                    <li><a class="nav-link" href="{{route('admin.promosi.index')}}">Promosi</a></li>
                    <li><a class="nav-link" href="{{route('admin.materi.index')}}">Materi</a></li>
                    <li><a class="nav-link" href="{{route('admin.workshop.index')}}">workshop</a></li>
                </ul>
            </li>

        </ul>

    </aside>
</div>
