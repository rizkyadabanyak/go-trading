@extends('admin.app')

@section('content')

<div class="main-content">
        <section class="section">

            <div class="section-header">
                <h1>Daftar Customer</h1>

            </div>
            <div class="card">
                <div class="card-header">
                    <h4><a href="{{route('admin.carousels.create')}}" class="btn btn-primary">TAMBAH DATA</a> </h4>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="item">
                            <thead>
                            <tr>
                                <th>img</th>
                                <th>Deskripsi</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </section>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#item').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('admin.carousels.index')}}",
                },
                columns: [
                    {
                        data: 'img',
                        name: 'img'
                    },
                    {
                        data: 'dsc',
                        name: 'dsc'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },

                ]
            });
        });
    </script>

@endsection
