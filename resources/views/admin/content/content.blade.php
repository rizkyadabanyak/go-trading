@extends('admin.app')

@section('content')
    <div class="main-content">
        <section class="section">

            <div class="section-header">
                <h1>Log Pembelian User</h1>

            </div>
            <div class="card">

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Name Customer</th>
                                <th>Nama Event</th>
                                <th>No Hp</th>
                                <th>Alamat</th>
                                <th>Profesi</th>
                                <th>Instansi</th>
                                <th>Jumlah Tiket</th>
                                <th>Total</th>
                                <th>no.rekening</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $a)
                                @if($a->user_id == Auth::user()->id && Auth::user()->level == 1)
                                    <tr>
                                        <td>
                                            {{ $loop->iteration}}
                                        </td>
                                        <td>{{$a->name_customer}}</td>
                                        <td>{{$a->name_event}}</td>
                                        <td>{{$a->no_hp}}</td>
                                        <td>{{$a->address}}</td>
                                        <td>{{$a->job}}</td>
                                        <td>{{$a->instansi}}</td>
                                        <td>{{$a->quantity}}</td>
                                        <td>{{$a->total_payment}}</td>
                                        <td>
                                            @if($a->status == 1)
                                                <span class="badge badge-warning">Belum Aktif</span>
                                            @else
                                                <span class="badge badge-success">Aktif</span>
                                            @endif
                                        </td>


                                        <td>
                                            @if($a->status == 1)
                                                <a href="/layouts/updatestatus/{{ $a->id }}" class="btn btn-danger">accept</a>
                                            @else
                                                <button type="button" class="btn btn-lg btn-primary" disabled>accept</button>
                                            @endif
                                        </td>
                                    </tr>
                                @elseif(Auth::user()->level == 0)
                                    <tr>
                                        <td>
                                            {{ $loop->iteration}}
                                        </td>
                                        <td>{{$a->name_customer}}</td>
                                        <td>{{$a->name_event}}</td>
                                        <td>{{$a->no_hp}}</td>
                                        <td>{{$a->address}}</td>
                                        <td>{{$a->job}}</td>
                                        <td>{{$a->instansi}}</td>
                                        <td>{{$a->quantity}}</td>
                                        <td>{{$a->total_payment}}</td>
                                        <td>{{$a->no_rekening}}</td>

                                        <td>
                                            @if($a->status == 1)
                                                <span class="badge badge-warning">Belum Aktif</span>
                                            @else
                                                <span class="badge badge-success">Aktif</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($a->status == 1)
                                                <a href="/layouts/updatestatus/{{ $a->id }}" class="btn btn-danger">accept</a>
                                            @else
                                                <button type="button" class="btn btn-lg btn-primary" disabled>accept</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </section>
    </div>
@endsection
