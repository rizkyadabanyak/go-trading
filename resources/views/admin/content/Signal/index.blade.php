@extends('admin.app')

@section('content')

<div class="main-content">
        <section class="section">

            <div class="section-header">
                <h1>Link</h1>

            </div>
            <div class="card">
                <div class="card-header">
                    <h4><a href="{{route('admin.signal.create')}}" class="btn btn-primary">TAMBAH DATA</a> </h4>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="item">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Link</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if($data == null)
                                <tr>
                                    <td>data kosong</td>
                                </tr>
                            @else
                                <tr>
                                    <td>{{$data->date}}</td>
                                    <td>{{$data->value}}</td>
                                    <td>
                                        <a href="{{route('admin.signal.edit',$data->id)}}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                        <a  href="{{route('admin.signalDestroy',$data->id)}}" class="btn btn-danger" onclick="return confirm(`Are you sure?`)"><i class="fa fa-trash"></i> Delete</a>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </section>
    </div>

@endsection
