@extends('admin.app')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Signal</h1>
            </div>
            <div class="card">

                <div class="card-body">
                    <form action="{{route('admin.signal.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Date</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="date" name="date" id="date" class="form-control @error('title') is-invalid @enderror">
                                @error('date')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Value</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="value" id="value" class="form-control @error('title') is-invalid @enderror">
                                @error('value')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary mr-1" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="section-body">
            </div>
        </section>
    </div>
@endsection
