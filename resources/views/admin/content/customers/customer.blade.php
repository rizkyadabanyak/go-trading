@extends('admin.app')

@section('content')

<div class="main-content">
        <section class="section">

            <div class="section-header">
                <h1>Daftar Customer</h1>

            </div>
            <div class="card">
                <div class="card-header">
                    <h4><a href="{{route('admin.customers.create')}}" class="btn btn-primary">TAMBAH DATA</a> </h4>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="item">
                            <thead>
                            <tr>
                                <th>Name Customer</th>
                                <th>City</th>
                                <th>Email</th>
                                <th>No Hp</th>
                                <th>Deposit</th>
                                <th>Penarikan</th>
                                <th>profil</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </section>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#item').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('admin.customers.index')}}",
                },
                columns: [
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'city',
                        name: 'city'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'whatsapp',
                        name: 'whatsapp'
                    },
                    {
                        data: 'deposit',
                        name: 'deposit'
                    },  {
                        data: 'penarikan',
                        name: 'penarikan'
                    },
                    {
                        data: 'img',
                        name: 'img'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },

                ]
            });
        });
    </script>

@endsection
