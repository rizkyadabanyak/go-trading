@extends('admin.app')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Add Customer</h1>

            </div>
            <div class="card">

                <div class="card-body">
                    <form action="{{route('admin.customers.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="name" id="name" value="{{$data->name}}" class="form-control @error('title') is-invalid @enderror">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">City</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="city" id="city" value="{{$data->city}}" class="form-control @error('title') is-invalid @enderror">
                                @error('city')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="email" id="email" value="{{$data->email}}" class="form-control @error('title') is-invalid @enderror">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Whatsapp</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="whatsapp" id="whatsapp" value="{{$data->whatsapp}}" class="form-control @error('title') is-invalid @enderror">
                                @error('whatsapp')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deposit</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="number" name="deposit" id="deposit" value="{{$data->deposit}}" class="form-control @error('title') is-invalid @enderror">
                                @error('deposit')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Penarikan</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="number" name="penarikan" id="penarikan" value="{{$data->penarikan}}" class="form-control @error('title') is-invalid @enderror">
                                @error('penarikan')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="file" name="image" id="image">
                                <input name="newImage" value="{{($data != null) ? $data->img : '' }}" hidden>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary mr-1" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="section-body">
            </div>
        </section>
    </div>
@endsection
