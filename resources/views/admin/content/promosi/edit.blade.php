@extends('admin.app')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Promosi</h1>

            </div>
            <div class="card">

                <div class="card-body">
                    <form action="{{route('admin.promosi.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="file" name="image" id="image">
                                <input name="newImage" value="{{($data != null) ? $data->img : '' }}" hidden>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">dsc</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="value" id="value" value="{{$data->value}}" class="form-control @error('title') is-invalid @enderror">
                                @error('value')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary mr-1" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="section-body">
            </div>
        </section>
    </div>
@endsection
