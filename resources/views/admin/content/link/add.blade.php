@extends('admin.app')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Link</h1>
            </div>
            <div class="card">

                <div class="card-body">
                    <form action="{{route('admin.link.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Link</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="link" id="link" class="form-control @error('title') is-invalid @enderror">
                                @error('link')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary mr-1" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="section-body">
            </div>
        </section>
    </div>
@endsection
