@extends('admin.app')

@section('content')

<div class="main-content">
        <section class="section">

            <div class="section-header">
                <h1>workshop</h1>

            </div>
            <div class="card">
                <div class="card-header">
                    <h4><a href="{{route('admin.workshop.create')}}" class="btn btn-primary">TAMBAH DATA</a> </h4>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="item">
                            <thead>
                            <tr>
                                <th>link</th>
                                <th>value</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </section>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#item').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{route('admin.workshop.index')}}",
            },
            columns: [
                {
                    data: 'link',
                    name: 'link'
                },
                {
                    data: 'value',
                    name: 'value'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },

            ]
        });
    });
</script>
@endsection
