<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name'	=> 'basic',
        ]);

        Category::create([
            'name'	=> 'intermediate',
        ]);
        Category::create([
            'name'	=> 'advance',
        ]);
    }
}
