<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::name('admin.')->middleware([])->group(function () {
    Route::resource('customers',\App\Http\Controllers\HomeController::class);
    Route::get('customers/{id}/destroy',[\App\Http\Controllers\HomeController::class,'destroy'])->name('customersDestroy');
    Route::get('customers/{id}/password',[\App\Http\Controllers\HomeController::class,'change'])->name('changePassword');
    Route::post('customers/{id}/password',[\App\Http\Controllers\HomeController::class,'changePassword'])->name('changePassword.post');

    Route::resource('carousels',\App\Http\Controllers\CarouselController::class);
    Route::get('carousels/{id}/destroy',[\App\Http\Controllers\CarouselController::class,'destroy'])->name('carouselsDestroy');

    Route::resource('link',\App\Http\Controllers\LinkController::class);
    Route::get('link/{id}/destroy',[\App\Http\Controllers\LinkController::class,'destroy'])->name('linkDestroy');

    Route::resource('promosi',\App\Http\Controllers\PromosiController::class);
    Route::get('promosi/{id}/destroy',[\App\Http\Controllers\PromosiController::class,'destroy'])->name('promosiDestroy');

    Route::resource('materi',\App\Http\Controllers\MateriController::class);
    Route::get('materi/{id}/destroy',[\App\Http\Controllers\MateriController::class,'destroy'])->name('materiDestroy');

    Route::resource('workshop',\App\Http\Controllers\WorkshopController::class);
    Route::get('workshop/{id}/destroy',[\App\Http\Controllers\WorkshopController::class,'destroy'])->name('workshopDestroy');

    Route::resource('notifikasi',\App\Http\Controllers\NotifikasiController::class);
    Route::get('notifikasi/{id}/destroy',[\App\Http\Controllers\NotifikasiController::class,'destroy'])->name('notifikasiDestroy');

    Route::resource('binomo',\App\Http\Controllers\BinomoController::class);
    Route::get('binomo/{id}/destroy',[\App\Http\Controllers\BinomoController::class,'destroy'])->name('binomoDestroy');

    Route::resource('signal',\App\Http\Controllers\SignalController::class);
    Route::get('signal/{id}/destroy',[\App\Http\Controllers\SignalController::class,'destroy'])->name('signalDestroy');

    Route::get('customer/{id}/img',[\App\Http\Controllers\Apiv1\ProfilController::class,'getImg'])->name('get.Img');
    Route::put('customer/{id}/img/',[\App\Http\Controllers\Apiv1\ProfilController::class,'img'])->name('img');

});
