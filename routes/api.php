<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('App\Http\Controllers\Api')->name('api.')->group(function () {
    Route::post('customer/login', [\App\Http\Controllers\Apiv1\AuthController::class, 'login'])->name('login');
    Route::post('customer/regis', [\App\Http\Controllers\Apiv1\AuthController::class, 'regis'])->name('regis');
    Route::get('customer/index', [\App\Http\Controllers\Apiv1\HomeController::class, 'index'])->name('index');
    Route::get('customer/binomo', [\App\Http\Controllers\Apiv1\HomeController::class, 'binomo'])->name('binomo');
    Route::get('customer/yutub', [\App\Http\Controllers\Apiv1\HomeController::class, 'yutub'])->name('yutub');
    Route::get('customer/count', [\App\Http\Controllers\Apiv1\HomeController::class, 'count'])->name('count');
    Route::get('customer/promosi', [\App\Http\Controllers\Apiv1\MateriController::class, 'promosi'])->name('promosi');
    Route::get('customer/workshop', [\App\Http\Controllers\Apiv1\MateriController::class, 'workshop'])->name('workshop');

    Route::namespace('App\Http\Controllers\Api')->middleware(['customers'])->group(function () {
        Route::get('customer/profile/{id}', [\App\Http\Controllers\Apiv1\ProfilController::class, 'getProfile'])->name('getProfile');
        Route::post('customer/profile/{id}', [\App\Http\Controllers\Apiv1\ProfilController::class, 'profile'])->name('profile');
        Route::post('customer/logout', [\App\Http\Controllers\Apiv1\AuthController::class, 'logout'])->name('logout');

        Route::get('customer/basic', [\App\Http\Controllers\Apiv1\MateriController::class, 'materi'])->name('materi');
        Route::get('customer/intermediate', [\App\Http\Controllers\Apiv1\MateriController::class, 'intermediate'])->name('intermediate');
        Route::get('customer/advance', [\App\Http\Controllers\Apiv1\MateriController::class, 'advance'])->name('advance');

        Route::get('customer/notifikasi/{id}', [\App\Http\Controllers\Apiv1\NoticeController::class, 'getNotifikasi'])->name('notifikasi');
        Route::get('customer/notifikasicount/{id}', [\App\Http\Controllers\Apiv1\NoticeController::class, 'getNotifikasiCount'])->name('notifikasiCount');
        Route::get('customer/signal', [\App\Http\Controllers\Apiv1\SignalController::class, 'signal'])->name('signal');
    });
});
